import 'package:flutter/material.dart';
void main()=>runApp(HelloFlutterApp());
class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}
String engLishGreeting = "Hello Flutter ";
String spanishGreeting = "Hola Flutter ";
String lgboGreeting = "Ndewo Flutter ";
String eweGreeting = "Mido gbe na Flutter ";
class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText =engLishGreeting;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed: (){
              setState(() {
                displayText = displayText==engLishGreeting? spanishGreeting:engLishGreeting;
              });
            },
                icon: Icon(Icons.brightness_auto_rounded )),

            IconButton(onPressed: (){
              setState(() {
                displayText = displayText==engLishGreeting? lgboGreeting:engLishGreeting;
              });
            },
                icon: Icon(Icons.brightness_auto_outlined )),
            IconButton(onPressed: (){
              setState(() {
                displayText = displayText==engLishGreeting? eweGreeting:engLishGreeting;
              });
            },
                icon: Icon(Icons.brightness_auto))
          ],


        ),
        body: Center(
          child: Text(
              displayText,
              style: TextStyle(fontSize: 24)),
        ),
      ),
    );
  }
}

